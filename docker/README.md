
## Building the docker image

Run under the folder of `carpark-prediction/`, 

```bash
docker build -f docker/Dockerfile . -t carpark-image-name
```

## Running the container and mounting the s3drive

`${HOME}/s3drive` in the host will be mounted to `/carpark/s3drive` in the container by the following command,

```bash
docker run -it -v ${HOME}/s3drive:/carpark/s3drive carpark-image-name
```